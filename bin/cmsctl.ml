open! Containers

let _ =
  match Sys.argv with
  | [| _ |] -> exit Cms.(main Generate ".")
  | [| _; "-w" |] -> exit Cms.(main Watch ".")
  | [| _; dir |] -> exit Cms.(main Generate dir)
  | [| _; "-w"; dir |] -> exit Cms.(main Watch dir)
  | _ ->
      prerr_endline "badargs";
      exit 1
