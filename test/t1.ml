open Containers
open Alcotest
open Cms__Utils

let adoc =
  ( module struct
    type t = Cms__Resource.adoc

    let equal = Cms__Resource.equal_adoc

    let pp = Cms__Resource.pp_adoc
  end : TESTABLE
    with type t = Cms__Resource.adoc )

let resource = (module Cms__Resource : TESTABLE with type t = Cms__Resource.t)

(* let load_registry () =
 *   let keys = [ "base.html"; "subdir/subdir_template.html" ] in
 *   Alcotest.(check (list string))
 *     "right_keys" keys
 *     ( Lwt_main.run
 *         (Cms__Template.load_registry "../../../test/correct_templates")
 *     |> Result.get_exn |> Dict.to_list |> List.map fst
 *     |> List.sort String.compare );
 *   Alcotest.(check (list string))
 *     "right_keys_trailing_slash" keys
 *     ( Lwt_main.run
 *         (Cms__Template.load_registry "../../../test/correct_templates/")
 *     |> Result.get_exn |> Dict.to_list |> List.map fst
 *     |> List.sort String.compare );
 *   Alcotest.(check (list string))
 *     "parse_error"
 *     [ "Mustache_parser.MenhirBasics.Error" ]
 *     ( match
 *         Lwt_main.run
 *           (Cms__Template.load_registry "../../../test/error_templates/")
 *       with
 *     | Error e -> e
 *     | Ok _ -> [] ) *)

let is_comment_delim () =
  check bool "is_delim" true (Cms__Adoc.is_comment_delim "////");
  check bool "is_not_delim" false (Cms__Adoc.is_comment_delim " //// ")

let _adocs : Cms__Resource.adoc array =
  [|
    {
      tags = [];
      category = "post";
      template = "base.html";
      link = "a";
      title = None;
      date = None;
      data = `Null;
    };
    {
      tags = [ "tag_a"; "tag_b" ];
      category = "post";
      template = "base.html";
      link = "b";
      title = None;
      date = None;
      data = `Null;
    };
    {
      tags = [ "tag_b"; "tag_c" ];
      category = "post";
      template = "base.html";
      link = "d";
      title = None;
      date = None;
      data = `Null;
    };
    {
      tags = [ "tag_c"; "tag_d" ];
      category = "page";
      template = "base.html";
      link = "e";
      title = None;
      date = None;
      data = `Null;
    };
  |]

let _resources =
  Cms__Resource.
    [
      Adoc ("a", _adocs.(0));
      Plain ("b", "b");
      Adoc ("c", _adocs.(1));
      Adoc ("d", _adocs.(2));
      Adoc ("e", _adocs.(3));
    ]

let empty_index () =
  check int "is_empty" 0 @@ Dict.cardinal (Cms__Index.make [])

let singleton_index () =
  let index = Cms__Index.make [ List.hd _resources ] in
  check (list string) "single_key" [ "post" ] (Dict.keys index |> List.of_iter);
  let cat = Dict.find "post" index in
  check string "category_name" "post" cat.category;
  check (list adoc) "category_all" [ _adocs.(0) ] cat.all;
  check int "category_per_tag_empty" 0 (Dict.cardinal cat.per_tag)

let multiple_index () =
  let index = Cms__Index.make _resources in
  check (list string) "categories" [ "page"; "post" ]
    (Dict.keys index |> List.of_iter);
  let post = Dict.find "post" index in
  let page = Dict.find "page" index in
  check (list adoc) "page_all" [ _adocs.(3) ] page.all;
  check (list adoc) "post_all" [ _adocs.(0); _adocs.(1); _adocs.(2) ] post.all;
  check (list string) "page_tags" [ "tag_c"; "tag_d" ]
    (Dict.keys page.per_tag |> List.of_iter);
  check (list string) "post_tags"
    [ "tag_a"; "tag_b"; "tag_c" ]
    (Dict.keys post.per_tag |> List.of_iter);
  check (list adoc) "page_tag_c" [ _adocs.(3) ] (Dict.find "tag_c" page.per_tag);
  check (list adoc) "page_tag_d" [ _adocs.(3) ] (Dict.find "tag_d" page.per_tag);

  check (list adoc) "post_tag_a" [ _adocs.(1) ] (Dict.find "tag_a" post.per_tag);
  check (list adoc) "post_tag_b" [ _adocs.(1); _adocs.(2) ]
    (Dict.find "tag_b" post.per_tag);
  check (list adoc) "post_tag_c" [ _adocs.(2) ] (Dict.find "tag_c" post.per_tag)

let () =
  run "cms"
    [
      (* ("Template", [ Alcotest.test_case "load_registry" `Quick load_registry ]); *)
      ("Adoc", [ test_case "is_comment_delim" `Quick is_comment_delim ]);
      ( "Index",
        [
          test_case "empty_index" `Quick empty_index;
          test_case "singleton_index" `Quick singleton_index;
          test_case "multiple_index" `Quick multiple_index;
        ] );
    ]
