type adoc_json = {
  tags : (string list[@default []]);
  category : string;
  title : (string option[@default None]);
  date : (string option[@default None]);
  template : string;
}
[@@deriving yojson { strict = false }]

open Containers
open Utils

type adoc = {
  tags : string list;
  category : string;
  template : string;
  link : string;
  title : string option;
  date : Date.t option;
  data : Jingoo.Jg_types.tvalue;
}

type tag = {
  category : string;
  template : string;
  link : string;
  data : Jingoo.Jg_types.tvalue;
}

type t =
  | Adoc of string * adoc
  | Tag of string * tag
  | Plain of string * string

let adoc_of_yojson link json =
  adoc_json_of_yojson json
  |> Result.map (fun (x : adoc_json) ->
         {
           tags = x.tags;
           category = x.category;
           template = x.template;
           link;
           title = x.title;
           date = Option.map Date.of_string x.date;
           data = Yojson.Safe.to_basic json |> Template.yojson_to_tvalue;
         })

let adoc_to_tvalue ({ link; data; _ } : adoc) =
  Jingoo.Jg_types.(box_obj [ ("link", box_string link); ("data", data) ])

let from_file : string -> string -> t or_errors Lwt.t =
 fun base path ->
  let link = String.drop (String.length base) path in
  match Filename.extension link with
  | ".adoc" -> (
      let link =
        match Filename.chop_extension link with "/index" -> "/" | x -> x
      in
      let* json = Adoc.front_matter path in
      match adoc_of_yojson link json with
      | Error err -> Lwt.return_error [ err ]
      | Ok metadata -> Lwt.return_ok (Adoc (path, metadata)) )
  | _ -> Lwt.return_ok (Plain (path, link))
