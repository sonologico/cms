open Utils

type registry

val load_registry : string -> registry or_errors Lwt.t

val render_to_file :
  file:string ->
  registry ->
  string ->
  (string * Jingoo.Jg_types.tvalue) list ->
  unit or_errors Lwt.t

val yojson_to_tvalue : Yojson.Basic.t -> Jingoo.Jg_types.tvalue
