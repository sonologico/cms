open Containers
open Fun
open Utils
open Jingoo

type registry = { templates : string Dict.t; env : Jg_types.environment }

let load_registry dir =
  let prefix_length = String.length dir in
  let* tree = Fileutils.ls_r dir in
  try%lwt
    Lwt_list.fold_left_s
      (fun acc path ->
        Fileutils.read_file path
        |> Lwt.map (fun x ->
               Dict.add
                 String.(drop prefix_length path |> drop_while (Char.equal '/'))
                 x acc))
      Dict.empty tree
    |> Lwt.map (fun templates ->
           Ok
             {
               templates;
               env = { Jg_types.std_env with template_dirs = [ dir ] };
             })
  with e -> Lwt.return_error [ Printexc.to_string e ]

let render reg key data =
  match Dict.find_opt key reg.templates with
  | None -> Error [ Format.sprintf "Template '%s' doesn't exist" key ]
  | Some template -> (
      try Ok (Jg_template.from_string ~env:reg.env ~models:data template)
      with e ->
        Error
          [
            Format.sprintf "Error rendering template '%s': '%s'" key
              (Printexc.to_string e);
          ] )

let render_to_file ~file reg key data =
  match render reg key data with
  | Error e -> Lwt.return_error e
  | Ok txt ->
      Lwt_io.with_file ~perm:0o644 ~mode:Lwt_io.Output file (fun ch ->
          Lwt_io.write ch txt |> Lwt.map (fun () -> Result.Ok ()))

let rec yojson_to_tvalue : Yojson.Basic.t -> Jingoo.Jg_types.tvalue = function
  | `Assoc xs -> Jg_types.box_obj (List.map (Pair.map2 yojson_to_tvalue) xs)
  | `List xs -> Jg_types.box_list (List.map yojson_to_tvalue xs)
  | `String x -> Jg_types.box_string x
  | `Int x -> Jg_types.box_int x
  | `Bool x -> Jg_types.box_bool x
  | `Null -> Jg_types.Tnull
  | `Float x -> Jg_types.box_float x
