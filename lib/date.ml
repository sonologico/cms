open Containers

type t = CalendarLib.Date.t

let equal = CalendarLib.Date.equal

let re =
  Re.(
    compile
      (seq
         [
           bol;
           group (repn digit 4 (Some 4));
           char '-';
           group (repn digit 2 (Some 2));
           char '-';
           group (repn digit 2 (Some 2));
           eol;
         ]))

let of_string s =
  let g = Re.exec re s in
  CalendarLib.Date.make
    (Re.Group.get g 1 |> int_of_string)
    (Re.Group.get g 2 |> int_of_string)
    (Re.Group.get g 3 |> int_of_string)

let of_string_opt s = try Some (of_string s) with Not_found -> None

let of_yojson = function
  | `String s -> Ok (of_string s)
  | _ -> Error "type error when reading date"

let to_string d =
  let open CalendarLib.Date in
  Format.sprintf "%d-%d-%d" (year d) (month d |> int_of_month) (day_of_month d)

let pp fmt x = Format.string fmt (to_string x)

let to_yojson d = `String (to_string d)
