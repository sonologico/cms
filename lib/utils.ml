open Containers

module Dict = struct
  include Map.Make (CCString)

  let to_yojson f x : Yojson.Safe.t =
    `Assoc (to_list x |> List.map (Pair.map2 f))

  let of_yojson f (x : Yojson.Safe.t) =
    match x with
    | `Assoc xs ->
        Result.(
          map_l
            (fun (k, v) ->
              match f v with Ok v -> Ok (k, v) | Error e -> Error e)
            xs
          >|= of_list)
    | _ -> Error "Type error. Expected an object"

  let pp a_pp fmt x = pp String.pp a_pp fmt x
end

type 'a or_errors = ('a, string list) result

let combine_result init f xs =
  List.fold_right
    (fun x acc ->
      match (acc, x) with
      | Ok a, Ok b -> Ok (f b a)
      | Error a, Error b -> Error (a @ b)
      | Error e, Ok _ | Ok _, Error e -> Error e)
    xs (Ok init)

let combine_result_lwt init f x = Lwt.map (combine_result init f) x

let catch_err p =
  try%lwt Lwt.map (fun x -> Ok x) p
  with e -> Lwt.return_error [ Printexc.to_string e ]

let cmd_res arr =
  Fileutils.command arr
  |> Lwt.map (function
       | Lwt_unix.WEXITED 0 -> Ok ()
       | _ ->
           Error [ "Error executing: " ^ String.concat " " (Array.to_list arr) ])

let ( let* ) = Lwt_result.( >>= )
