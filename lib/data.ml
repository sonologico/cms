open! Containers
open Utils

let load (dir : string) : (string * Jingoo.Jg_types.tvalue) list or_errors Lwt.t
    =
  let* files = Fileutils.ls_r dir in
  Lwt_result.ok
    (Lwt_list.map_p
       (fun x ->
         let%lwt content = Fileutils.read_file x in
         let key = Filename.(basename x |> remove_extension) in
         Lwt.return
           (key, Yojson.Basic.from_string content |> Template.yojson_to_tvalue))
       files)
