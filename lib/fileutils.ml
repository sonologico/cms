open Containers

let replace_prefix base new_base src =
  String.drop (String.length base) src |> Filename.concat new_base

let ls path =
  let%lwt dh = Lwt_unix.opendir path in
  let batch_size = 1000 in
  let cleanup () = Lwt_unix.closedir dh in
  let rec loop acc () =
    let%lwt arr = Lwt_unix.readdir_n dh batch_size in
    let acc = arr :: acc in
    if Array.length arr < batch_size then
      Lwt.return (Array.concat acc |> Array.to_list)
    else loop (arr :: acc) ()
  in
  Lwt.finalize (loop []) cleanup

let rec ls_r src =
  let%lwt st = Lwt_unix.stat src in
  match st.st_kind with
  | S_REG -> Lwt.return_ok [ src ]
  | S_DIR ->
      let%lwt children = ls src in
      Lwt_list.filter_map_p
        (fun file ->
          match file.[0] with
          | '.' | '_' -> Lwt.return_none
          | _ -> Lwt.map Option.pure (ls_r (Filename.concat src file)))
        children
      |> Lwt.map (fun children ->
             Result.flat_map
               (fun children -> Ok (List.concat children))
               (Result.flatten_l children))
  | _ -> Lwt.return_error [ Format.sprintf "Unexpected file type at '%s'" src ]

let command c = Lwt_process.with_process_none (c.(0), c) (fun p -> p#status)

let command_full c =
  Lwt_process.with_process_full
    (c.(0), c)
    (fun p ->
      let%lwt out = Lwt_stream.to_string (Lwt_io.read_chars p#stdout) in
      let%lwt err = Lwt_stream.to_string (Lwt_io.read_chars p#stderr) in
      Lwt.map (fun ec -> (ec, out, err)) p#status)

let tmpdir mode prefix =
  let path =
    Filename.(
      concat (get_temp_dir_name ())
        (prefix ^ (Unix.time () |> Float.to_int |> Int.to_string)))
  in
  Lwt_unix.mkdir path mode |> Lwt.map (fun () -> path)

let mkdir_p path =
  Lwt.map
    (function
      | Lwt_unix.WEXITED 0 -> Ok ()
      | _ -> Error [ Format.sprintf "failure creating '%s'" path ])
    (command [| "mkdir"; "-m755"; "-p"; path |])

let copy src dst =
  Lwt.map
    (function
      | Lwt_unix.WEXITED 0 -> Ok ()
      | _ -> Error [ Format.sprintf "failure copying '%s' to '%s'" src dst ])
    (command [| "cp"; src; dst |])

let read_file file =
  Lwt_io.with_file ~mode:Lwt_io.input file (fun ch ->
      Lwt_io.read_chars ch |> Lwt_stream.to_string)

let md5_dirs dirs =
  let cmd =
    Array.concat
      [ [| "find" |]; dirs; [| "-type"; "f"; "-exec"; "md5sum"; "{}"; "+" |] ]
  in
  fun () ->
    let%lwt ec, out, err = command_full cmd in
    match ec with
    | Unix.WEXITED 0 -> Lwt.return_ok (Digest.string out)
    | _ -> Lwt.return_error (Format.sprintf "Error computing md5: '%s'" err)

exception Watch_stop

let watch_dirs (dirs : string array) (f : unit -> unit Lwt.t) : unit Lwt.t =
  let md5 = md5_dirs dirs in
  let rec p prev_hash =
    try%lwt
      Lwt.bind (md5 ()) @@ function
      | Error e -> failwith e
      | Ok hash ->
          ( match prev_hash with
          | Some prev when Digest.equal hash prev -> Lwt.return_unit
          | _ -> f () );%lwt
          Lwt_unix.sleep 5.0;%lwt
          p (Some hash)
    with
    | Watch_stop -> Lwt.return_unit
    | e ->
        Lwt_io.printl (Printexc.to_string e);%lwt
        Lwt_unix.sleep 5.0;%lwt
        p prev_hash
  in
  p None
