open Containers
open Utils
open Jingoo

let doc_compare a b =
  let open Resource in
  let compare_date a b =
    Option.compare CalendarLib.Date.compare a.date b.date
  in
  let compare_title a b =
    Option.compare
      String.(fun a b -> compare (lowercase_ascii a) (lowercase_ascii b))
      a.title b.title
  in
  let compare_link (a : Resource.adoc) (b : Resource.adoc) =
    String.compare_natural (Filename.basename a.link) (Filename.basename b.link)
  in
  match compare_date a b with
  | 0 -> ( match compare_title a b with 0 -> compare_link a b | x -> x )
  | x -> x

module Category = struct
  let add k v m =
    Dict.update k (function None -> Some [ v ] | Some xs -> Some (v :: xs)) m

  type t = {
    category : string;
    all : Resource.adoc list;
    per_tag : Resource.adoc list Dict.t;
  }

  let empty category = { category; all = []; per_tag = Dict.empty }

  let add_doc (doc : Resource.adoc) x =
    {
      category = x.category;
      all = doc :: x.all;
      per_tag =
        List.fold_left (fun acc tag -> add tag doc acc) x.per_tag doc.tags;
    }

  let singleton category doc = empty category |> add_doc doc

  let to_tvalue { category; all; per_tag } =
    Jg_types.(
      box_obj
        [
          ("category", box_string category);
          ("all", box_list (List.map Resource.adoc_to_tvalue all));
          ( "per_tag",
            Dict.to_list per_tag
            |> List.map (fun (k, v) ->
                   (k, List.map Resource.adoc_to_tvalue v |> box_list))
            |> Jg_types.box_obj );
        ])
end

let make (tree : Resource.t list) : Category.t Dict.t =
  let adocs =
    List.filter_map (function Resource.Adoc (_, x) -> Some x | _ -> None) tree
    |> List.sort doc_compare
  in
  List.fold_right
    (fun (x : Resource.adoc) acc ->
      Dict.update x.category
        (function
          | None -> Some (Category.singleton x.category x)
          | Some xs -> Some (Category.add_doc x xs))
        acc)
    adocs Dict.empty

let to_tvalue (index : Category.t Dict.t) : Jg_types.tvalue =
  Dict.to_list index
  |> List.map (fun (k, v) -> (k, Category.to_tvalue v))
  |> Jg_types.box_obj
