open Containers
open Fun

let render_file_to_string file =
  Lwt.map
    (fun (_, x, _) -> x)
    (Fileutils.command_full
       [|
         "asciidoctor";
         "-b";
         "html5";
         "-d";
         "book";
         "-e";
         "-a";
         "showtitle";
         "--out-file=-";
         file;
       |])

let is_comment_delim =
  Re.execp Re.(compile (seq [ bol; repn (char '/') 4 None; eol ]))

let front_matter file =
  Lwt_io.with_file ~mode:Lwt_io.Input file (fun ch ->
      let stream = Lwt_io.read_lines ch in
      match%lwt Lwt_stream.get stream with
      | Some first when is_comment_delim first -> (
          let%lwt lines =
            Lwt_stream.get_while (negate is_comment_delim) stream
          in
          try Lwt.return_ok (Yojson.Safe.from_string (String.concat "\n" lines))
          with e -> Lwt.return_error [ Printexc.to_string e ] )
      | _ -> Lwt.return_error [ "Missing front matter" ])
