open Containers
open Utils

type paths = {
  content : string;
  templates : string;
  data : string;
  output : string;
}

let write templates dir tree data : unit or_errors Lwt.t =
  let get_dst_ensure_dir suffix =
    let dst = Filename.concat dir suffix in
    Lwt_result.map (fun () -> dst) (Fileutils.mkdir_p (Filename.dirname dst))
  in
  let write = function
    | Resource.Plain (path, link) ->
        let* dst = get_dst_ensure_dir link in
        Fileutils.copy path dst
    | Resource.Tag _ -> assert false
    | Resource.Adoc (path, metadata) ->
        let* dst =
          get_dst_ensure_dir (Filename.concat metadata.link "index.html")
        in
        let%lwt content = Adoc.render_file_to_string path in
        Template.render_to_file ~file:dst templates metadata.template
          ( ("content", Jingoo.Jg_types.box_string content)
          :: ("data", metadata.data) :: data )
  in
  Lwt_list.map_p write tree |> combine_result_lwt () (fun () () -> ())

let transform { content; templates; output; data; _ } =
  let* templates = Template.load_registry templates in
  let* data = Data.load data in
  let* files = Fileutils.ls_r content in
  let* resources =
    Lwt_list.map_p (Resource.from_file content) files
    |> combine_result_lwt [] List.cons
  in
  let index = Index.make resources |> Index.to_tvalue in
  let* tmp = catch_err (Fileutils.tmpdir 0o755 "cms_tree") in
  let* () = write templates tmp resources (("index", index) :: data) in
  let* () = cmd_res [| "rm"; "-rf"; output |] in
  let* () = cmd_res [| "mv"; tmp; output |] in
  cmd_res [| "rm"; "-rf"; tmp |]

let watch server ({ content; templates; output; _ } as paths) =
  if server then Lwt.async (fun () -> Dev_server.run output);
  ( Fileutils.watch_dirs [| content; templates |] @@ fun () ->
    Lwt_io.printl "Generating...";%lwt
    match%lwt transform paths with
    | Result.Ok () -> Lwt_io.printl "Generated"
    | Result.Error errs -> Lwt_list.iter_s Lwt_io.printl errs );%lwt
  Lwt.return_ok ()

type cmd = Watch | Generate

let make_paths dir =
  {
    content = Filename.concat dir "content";
    templates = Filename.concat dir "templates";
    output = Filename.concat dir "_build";
    data = Filename.concat dir "data";
  }

let main cmd dir =
  let paths = make_paths dir in
  let p =
    match cmd with Watch -> watch true paths | Generate -> transform paths
  in
  match Lwt_main.run p with
  | Ok () -> 0
  | Error es ->
      List.iter prerr_endline es;
      1
