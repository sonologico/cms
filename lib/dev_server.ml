open! Containers

let run dir =
  let open Cohttp_lwt_unix in
  let callback _conn req _body =
    let uri = Request.uri req in
    let uri =
      match Filename.extension (Uri.path uri) with
      | "" -> Uri.to_string uri |> (fun x -> x ^ "/index.html") |> Uri.of_string
      | _ -> uri
    in

    let fname = Server.resolve_local_file ~docroot:dir ~uri in
    Server.respond_file ~fname ()
  in
  Server.create ~mode:(`TCP (`Port 4242)) (Server.make ~callback ())
