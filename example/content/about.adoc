////
{
  "template": "page.html",
  "title": "About",
  "category": "page"
}
////

= About

Raphael Sousa Santos is a composer, visual artist and programmer based in the Netherlands. He experiments with the development of programming languages and the use of random processes, evolutionary algorithms and spectral techniques for the creation and performance of music and visual arts.

His research interest spans audio analysis and synthesis, music theory and computer assisted composition with a focus on the role of abstraction, analogy, syntactic manipulation and the use of language during the compositional or analytical process.

Besides developing software for audio and music, Raphael Sousa Santos also has experience developing distributed systems handling high amounts of traffic and data.

Co-founder of Gamag, a globally distributed music research group.

For contact, send an email to {first name} at sonologi.co.

http://github.com/sonologico
